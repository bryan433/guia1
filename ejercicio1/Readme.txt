-El programa consiste en un registro de profesores, al iniciar este programa debemos ingresar la cantidad de docentes que queremos añadir, luego de esto se nos pedira el nombre y la edad de cada uno, despues se calculara el promedio de las edades de cada profesor, y se mostrara por pantalla cual es menor, mayor o igual, al promedio, tambien podremos ver quien es el mas joven y el mayor.

-Para usar este programa primero debemos descargarlo, para eso copiaremos el link,
luego de eso abrimos la terminal, y colocamos esto, git clone https://gitlab.com/bryan433/guia1, al hacer esto vamos a clonar el repositorio con todo lo que esta dentro, luego abrimos la terminal desde donde esta nuestra carpeta, luego colocamos make para poder compilar, cuando podamos compilar este programa, deberemos colocar en la terminar ./programa, y se nos inicializara.

Sistema operativo:
Debian GNU/Linux 9 (stretch) 64-bit 

Autor: Bryan Ahumada
