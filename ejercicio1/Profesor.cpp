#include <iostream>
#include "Profesor.h"

using namespace std;

Profesor::Profesor(){
}

void Profesor::agregar_datos_profesor(){
    cout << "Ingrese el nombre del profesor: ";
    getline(cin, this->nombre);

    cout << "Ingrese la edad del profesor: ";
    cin >> this->edad;

    while(getchar() != '\n');

    cout << "\n";
}

int Profesor::get_edad(){
    return this->edad;
}


string Profesor::get_nombre(){
    return this->nombre;
}
