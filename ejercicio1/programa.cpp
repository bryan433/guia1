#include <iostream>
#include "Profesor.h"
#include "Promedio.h"

using namespace std;

int main(int argc, char *argv[]){
    int num_profesores = 0; 
	
	cout <<"ingrese el numero de profesores que va a añadir al programa: " << endl;
	cin >> num_profesores >> endl;	
	
    cout << "\n\tREGISTRO PROFESORES\n" << endl;

    Promedio promedio = Promedio(num_profesores);

    promedio.llenar_arreglo();
    promedio.calcular_promedio();
    promedio.mayor_menor_promedio();
    promedio.profesor_joven();
    promedio.profesor_mayor();

    return 0;
}
