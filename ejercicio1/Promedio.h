#include <iostream>
#include "Profesor.h"

using namespace std;
 
#ifndef PROMEDIO_H
#define PROMEDIO_H

class Promedio{
    private:
        int num_profesores;
        float promedio = 0;
        Profesor *profesores = NULL;

    public:
        Promedio(int num_profesores);
        void llenar_arreglo();
        void calcular_promedio();
        void mayor_menor_promedio();
        void profesor_joven();
        void profesor_mayor();
        void set_promedio(float promedio);
        float get_promedio();
        
};
#endif
