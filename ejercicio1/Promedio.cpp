#include <iostream>
#include <stdio.h>
#include "Profesor.h"
#include "Promedio.h"

using namespace std;

Promedio::Promedio(int num_profesores){
    this->num_profesores = num_profesores;
    this->profesores = new Profesor [num_profesores];
}


void Promedio::llenar_arreglo(){
    for (int i = 0; i < num_profesores; i++){
        this->profesores[i].agregar_datos_profesor();
    }
}


void Promedio::calcular_promedio(){
    for (int i = 0; i < this->num_profesores; i++){
         this->promedio += this->profesores[i].get_edad();
    }

    set_promedio(promedio/num_profesores);     
    
    system("clear");

    cout << "Edad promedio: " << get_promedio() << "\n" << endl;

}


void Promedio::mayor_menor_promedio(){
     int menor = 0;
     int mayor = 0;
     int igual = 0;

     for (int i = 0; i < this->num_profesores; i++){
        if (this->profesores[i].get_edad() < get_promedio()){
            menor++;
        }
        
        else if(this->profesores[i].get_edad() > get_promedio()){
            mayor++;
        }
    
        else{
            igual++;
        }
    }
    
    cout << "Hay " << menor << " profesores con edad menor al promedio" << endl;
    cout << "Hay " << mayor << " profesores con edad mayor al promedio" << endl;
    cout << "Hay " << igual << " profesores con edad igual al promedio \n" << endl;

}


void Promedio::profesor_joven(){
    int menor = 100;
    int indice_menor;

    for (int i = 0; i < this->num_profesores; i++){
        if(this->profesores[i].get_edad() < menor){
            menor = this->profesores[i].get_edad();
            indice_menor = i;
        }
    }
 
    cout << "Profesor más joven --> " << this->profesores[indice_menor].get_nombre() << "\n" << endl;

}


void Promedio::profesor_mayor(){
    int mayor = 0;
    int indice;

    for (int i = 0; i < this->num_profesores; i++){
        if(this->profesores[i].get_edad() > mayor){
            mayor = this->profesores[i].get_edad();
            indice = i;
        }
    }
 
    cout << "Profesor de mayor edad --> " << this->profesores[indice].get_nombre() << "\n" << endl;

}


void Promedio::set_promedio(float promedio){
    this->promedio = promedio;
}


float Promedio::get_promedio(){
    return this->promedio;
}
