#include <iostream>

using namespace std;

#ifndef PROFESOR_H
#define PROFESOR_H

class Profesor{
	private:
		int edad;
		string nombre;
	
	
	public:
		
		Profesor();
		void agregar_datos_profesor();
		int get_edad();
		string get_nombre();

};
#endif

