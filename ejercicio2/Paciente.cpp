#include <iostream>
#include "Paciente.h"
#include "Domicilio.h"

using namespace std;

Paciente::Paciente(){
}

void Paciente::add_datos_paciente(){
    int opcion;

    cout << "Ingrese el nombre del paciente: ";
    getline(cin, this->nombre);

    cout << "Ingrese la edad del paciente: ";
    cin >> this->edad;

    while(getchar() != '\n');

    cout << "Ingrese el sexo del paciente [Femenino / Masculino / Otro] \n --> ";
    getline(cin, this->sexo);

    cout << "Ingrese el teléfono del paciente: ";
    getline(cin, this->telefono);
    
    cout << "¿Tiene seguro? \n [1] Si\n [2] No\n";
    cin >> opcion;

    if(opcion == 1){
        set_seguro(true);
    }

    else if(opcion == 2){
        set_seguro(false);
    }

    while(getchar() != '\n');

    this->domicilio = Domicilio();
    this->domicilio.add_datos_domicilio();

    cout << "\n";
}

void Paciente::set_seguro(bool seguro){
    this->seguro = seguro;
}


int Paciente::get_edad(){
    return this->edad;
}

bool Paciente::get_seguro(){
    return this->seguro;
}

string Paciente::get_nombre(){
    return this->nombre;
}

string Paciente::get_telefono(){
    return this->telefono;
}

string Paciente::get_sexo(){
    return this->sexo;
}
Domicilio Paciente::get_domicilio(){
    return this->domicilio;
}
