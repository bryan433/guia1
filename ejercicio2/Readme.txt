-Empezando: Al empezar el programa deberemos colocar el numero de pacientes que deseamos registrar, luego de eso se nos daran 7 opciones las que consistiran, en registrar el paciente, en la cual se pedira el nombre, la edad, el sexo, la calle, el numero de la calle y en que ciudad vive, y tambien si esque tiene seguro. La segunda opcion nos dejara ver la cantidad de pacientes que estan registrados en el hospital. La tercera opcion puede buscar a un paciente por su nombre y ver todos sus datos. La cuarta opcion es el porcentaje de pacientes por sexo. La quinta opcion nos mostrara el porcentaje de personas con seguro que tienen o no. La sexta opcion nos muestra un porcentaje de pacientes por edad, niños (los cuales son menor a 13 años), jovenes(menores a 30 años y mayores a 13 años) y adultos (mayores de 30 años). la ultima opcion es la que nos cerrara el programa.

-Para usar este programa primero debemos descargarlo, para eso copiaremos el link,
luego de eso abrimos la terminal, y colocamos esto, git clone https://gitlab.com/bryan433/guia1, al hacer esto vamos a clonar el repositorio con todo lo que esta dentro, luego abrimos la terminal desde donde esta nuestra carpeta, luego colocamos make para poder compilar, cuando podamos compilar este programa, deberemos colocar en la terminar ./programa, y se nos inicializara.

Sistema operativo:
Debian GNU/Linux 9 (stretch) 64-bit 

Autor: Bryan Ahumada 
