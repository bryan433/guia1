#include <iostream>
#include "Paciente.h"

using namespace std;

#ifndef SECRETARIA_H
#define SECRETARIA_H

class Secretaria{
    private:
        Paciente *pacientes = NULL;
        int num_pacientes;
        float porcentaje_femenino = 0;
        float porcentaje_masculino = 0;
        float porcentaje_otro = 0;
        float porcentaje_con_seguro = 0;
        float porcentaje_sin_seguro = 0;
        float porcentaje_menor = 0;
        float porcentaje_joven = 0;
        float porcentaje_adulto = 0;

    public:
        Secretaria(int num_pacientes);
        void registrar_pacientes();
        void imprimir_hospitalizados();
        void imprimir_un_paciente();
        void porcentaje_edad();                
        void porcentaje_sexo();
        void porcentaje_seguro();

        float get_porcentaje_femenino();
        float get_porcentaje_masculino();
        float get_porcentaje_otro();

        float get_porcentaje_con_seguro();
        float get_porcentaje_sin_seguro();

        float get_porcentaje_menor();
        float get_porcentaje_adulto();
        float get_porcentaje_joven();
       
};
#endif
