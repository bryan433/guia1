#include <iostream>

using namespace std;
#ifndef DOMICILIO_H
#define DOMICILIO_H

class Domicilio{
    private:
        string calle;
        string ciudad;
        int numero;
    
    public:
        Domicilio();
        void add_datos_domicilio();
        int get_numero();
        string get_calle();
        string get_ciudad();
};
#endif
