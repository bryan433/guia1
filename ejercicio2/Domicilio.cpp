#include <iostream>
#include "Domicilio.h"

using namespace std;

Domicilio::Domicilio(){
}

void Domicilio::add_datos_domicilio(){
    cout << "¿En qué calle vive? " << endl;
    getline(cin , this->calle);
    
    cout << "Ingrese el número de la calle: ";
    cin >> this->numero;

    cout << "¿A qué ciudad pertenece?" << endl;
    cin >> this->ciudad;

}


string Domicilio::get_calle(){
    return this->calle;
}


string Domicilio::get_ciudad(){
    return this->ciudad;
}


int Domicilio::get_numero(){
    return this->numero;
}
