#include <iostream>
#include "Paciente.h"
#include "Domicilio.h"
#include "Secretaria.h"

using namespace std;

Secretaria::Secretaria(int num_pacientes){
    this->num_pacientes = num_pacientes;
    this->pacientes = new Paciente[this->num_pacientes];
}

void Secretaria::registrar_pacientes(){
    for (int i = 0; i < this->num_pacientes; i++){
        this->pacientes[i].add_datos_paciente();

        cout << '\n';
        while(getchar() != '\n');
    }
}

void Secretaria::imprimir_hospitalizados(){
    cout << "\tPACIENTES HOSPITALIZADOS" << endl;
    for(int i = 0; i < this->num_pacientes; i++){
        if (pacientes[i].get_nombre() == "\0"){
            cout << "\t - No hay pacientes hospitalizados" << endl;
        }

        else{
            cout << "\t -" << this->pacientes[i].get_nombre() << endl;
        }
    }
}

void Secretaria::imprimir_un_paciente(){
    string buscado;

    cout << "Ingrese el nombre del paciente que busca: ";
    getline(cin, buscado);
    
    for(int i = 0; i < this->num_pacientes; i++){
        if(pacientes[i].get_nombre() == buscado){
            cout << "Nombre: " << this->pacientes[i].get_nombre() << endl;
            cout << "Edad: " << this->pacientes[i].get_edad() << endl;
            cout << "Teléfono: " << this->pacientes[i].get_telefono() << endl;
            cout << "Sexo: " << this->pacientes[i].get_sexo() << endl;

            if(this->pacientes[i].get_seguro() == true){
                cout << "Seguro: Si" << '\n' << endl;
            }

            else if(this->pacientes[i].get_seguro() == false){
                cout << "Seguro: No" << '\n' << endl;
            }
                
            cout << "Calle: " << this->pacientes[i].get_domicilio().get_calle() << endl;
            cout << "Número de casa:" << this->pacientes[i].get_domicilio().get_numero() << endl;
            cout << "Ciudad: " << this->pacientes[i].get_domicilio().get_ciudad() << '\n' << endl;
        }
        
        else{
            cout << " EL PACIENTE NO ESTA REGISTRADO" << endl;
        }
    }
}

void Secretaria::porcentaje_sexo(){
    float masculino = 0;
    float femenino = 0;
    float otro = 0;

    for(int i = 0; i < this->num_pacientes; i++){
        if(this->pacientes[i].get_sexo() == "Femenino" || this->pacientes[i].get_sexo() == "femenino"){
            femenino++;
        }

        else if (this->pacientes[i].get_sexo() == "Maculino" || this->pacientes[i].get_sexo() == "masculino"){
            masculino++;
        }
       
        else if (pacientes[i].get_sexo() == "\0"){
            cout << "No hay pacientes hospitalizados" << endl;
        }
            
        else{
            otro++;
        }
    }

    this->porcentaje_femenino = (femenino/this->num_pacientes) * 100;
    this->porcentaje_masculino = (masculino/this->num_pacientes) * 100;
    this->porcentaje_otro = (otro/this->num_pacientes) * 100;

    cout << "Porcentaje mujeres: " << get_porcentaje_femenino() << "%" << endl;
    cout << "Porcentaje hombre: " << get_porcentaje_masculino() << "%" << endl;
    cout << "Porcentaje otro: " << get_porcentaje_otro() << "%" << endl;

}
    

void Secretaria::porcentaje_seguro(){
    float con_seguro = 0;
    float sin_seguro = 0;

    for(int i = 0; i < this->num_pacientes; i++){
        if(this->pacientes[i].get_seguro() == false && this->pacientes[i].get_nombre() == "\0"){ 
            cout << "No hay pacientes hospitalizados" << endl;
            con_seguro = 0;
            sin_seguro = 0;
        }

        else if(this->pacientes[i].get_seguro() == true){
            con_seguro++;
        }

        else if(pacientes[i].get_seguro() == false){
            sin_seguro++;
        }
    }

    this->porcentaje_con_seguro = (con_seguro/this->num_pacientes) * 100;
    this->porcentaje_sin_seguro = (sin_seguro/this->num_pacientes) * 100;

    cout << "Porcentaje con seguro: " << get_porcentaje_con_seguro() << "%" << endl;
    cout << "Porcentaje sin seguro: " << get_porcentaje_sin_seguro() << "%" << endl;

}

void Secretaria::porcentaje_edad(){
    float menor = 0;
    float joven = 0;
    float adulto = 0;

    for(int i = 0; i < this->num_pacientes; i++){
        
        if(this->pacientes[i].get_edad() > 13 && this->pacientes[i].get_edad() < 30){
            joven++;
        }

        else if (this->pacientes[i].get_edad() == 0 && this->pacientes[i].get_nombre() == "\0"){
            cout << "No hay pacientes hospitalizados" << endl;
            menor = 0;
            joven = 0;
            adulto = 0;
        }

        else if(this->pacientes[i].get_edad() <= 13){
            menor++;
        }
    
        else if(this->pacientes[i].get_edad() > 30){
            adulto++;
        }
        
    }

    this->porcentaje_menor = (menor/this->num_pacientes) * 100;
    this->porcentaje_joven = (joven/this->num_pacientes) * 100;
    this->porcentaje_adulto = (adulto/this->num_pacientes) * 100;

    cout << "Porcentaje niños hasta 13 años: " << get_porcentaje_menor() << "%" << endl;
    cout << "Porcentaje jovenes: " << get_porcentaje_joven() << "%" << endl;
    cout << "Porcentaje adultos: " << get_porcentaje_adulto() << "%" << endl;

}

float Secretaria::get_porcentaje_femenino(){
    return this->porcentaje_femenino;
} 

float Secretaria::get_porcentaje_masculino(){
    return this->porcentaje_masculino;
} 

float Secretaria::get_porcentaje_otro(){
    return this->porcentaje_otro;
} 

float Secretaria::get_porcentaje_con_seguro(){
    return this->porcentaje_con_seguro;
}
 
float Secretaria::get_porcentaje_sin_seguro(){
    return this->porcentaje_sin_seguro;
} 

float Secretaria::get_porcentaje_menor(){
    return this->porcentaje_menor;
} 

float Secretaria::get_porcentaje_adulto(){
    return this->porcentaje_adulto;
} 

float Secretaria::get_porcentaje_joven(){
    return this->porcentaje_joven;
} 
